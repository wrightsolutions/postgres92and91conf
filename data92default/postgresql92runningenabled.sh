#!/bin/sh
# Enabled check plus cursory check of postgresql data directory
pgdata='/var/lib/pgsql/data'
CTLBIN_=/usr/bin/systemctl
ls -lk /etc/systemd/system/multi-user.target.wants/postgresql.service
wc -l /etc/systemd/system/multi-user.target.wants/postgresql.service
${CTLBIN_} is-enabled postgresql.service
enabled_rc=$?
more $pgdata/PG_VERSION
[ $enabled_rc -gt 0 ] && printf "Warning: postgresql is NOT currently enabled.\n"
${CTLBIN_} status postgresql.service
/usr/bin/postgresql-check-db-dir $pgdata
printf "Correct permissions for centos postgresql look something like below\n"
C600="-rw------- 1 postgres postgres 19845 Dec 19 19:09"
printf "%s /var/lib/pgsql/data/postgresql.conf\n" "$C600"
printf "If your .conf file is located in system default /var/lib/pgsql/data then shown next:\n"
vdir -lk /var/lib/pgsql/data/postgresql.conf
printf "When postgresql is running there should be a pid file in data directory (?/var/lib/pgsql/data?) - shown next if exists\n"
/usr/bin/find /var/ /opt/ -type f -name 'postmaster.pid' -size -2 -printf "%t %p\n"
printf "When postgresql is running there should be a lock file below /run/ - shown next if exists\n"
/usr/bin/find /run/ -type f -name '*PGSQL*' -printf "%t %p\n"


